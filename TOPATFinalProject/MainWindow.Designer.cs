﻿namespace TOPATFinalProject {
    partial class MainWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.doLexerBtn = new System.Windows.Forms.Button();
            this.openFileBtn = new System.Windows.Forms.Button();
            this.grammarGroupBox = new System.Windows.Forms.GroupBox();
            this.grammarRichTextBox = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.parseBtn = new System.Windows.Forms.Button();
            this.lexerGroupBox = new System.Windows.Forms.GroupBox();
            this.lexerRichTextBox = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.parsingGroupBox = new System.Windows.Forms.GroupBox();
            this.parseTreeRichTextBox = new System.Windows.Forms.RichTextBox();
            this.translateBtn = new System.Windows.Forms.Button();
            this.parseTreeGroupBox = new System.Windows.Forms.GroupBox();
            this.parseTreeView = new System.Windows.Forms.TreeView();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.saveBtn = new System.Windows.Forms.Button();
            this.cppGroupBox = new System.Windows.Forms.GroupBox();
            this.intermediateLanguageRichTextBox = new System.Windows.Forms.RichTextBox();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.grammarGroupBox.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.lexerGroupBox.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.parsingGroupBox.SuspendLayout();
            this.parseTreeGroupBox.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.cppGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 339);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(809, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(70, 17);
            this.toolStripStatusLabel.Text = "Status Label";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(809, 339);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.doLexerBtn, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.openFileBtn, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.grammarGroupBox, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(196, 333);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // doLexerBtn
            // 
            this.doLexerBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doLexerBtn.Location = new System.Drawing.Point(3, 301);
            this.doLexerBtn.Name = "doLexerBtn";
            this.doLexerBtn.Size = new System.Drawing.Size(190, 29);
            this.doLexerBtn.TabIndex = 0;
            this.doLexerBtn.Text = "Do Lexical Analysis";
            this.doLexerBtn.UseVisualStyleBackColor = true;
            this.doLexerBtn.Click += new System.EventHandler(this.doLexerBtn_Click);
            // 
            // openFileBtn
            // 
            this.openFileBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.openFileBtn.Location = new System.Drawing.Point(3, 3);
            this.openFileBtn.Name = "openFileBtn";
            this.openFileBtn.Size = new System.Drawing.Size(190, 29);
            this.openFileBtn.TabIndex = 1;
            this.openFileBtn.Text = "Open File";
            this.openFileBtn.UseVisualStyleBackColor = true;
            this.openFileBtn.Click += new System.EventHandler(this.openFileBtn_Click);
            // 
            // grammarGroupBox
            // 
            this.grammarGroupBox.Controls.Add(this.grammarRichTextBox);
            this.grammarGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grammarGroupBox.Location = new System.Drawing.Point(3, 38);
            this.grammarGroupBox.Name = "grammarGroupBox";
            this.grammarGroupBox.Size = new System.Drawing.Size(190, 257);
            this.grammarGroupBox.TabIndex = 2;
            this.grammarGroupBox.TabStop = false;
            this.grammarGroupBox.Text = "Grammar";
            // 
            // grammarRichTextBox
            // 
            this.grammarRichTextBox.DetectUrls = false;
            this.grammarRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grammarRichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grammarRichTextBox.Location = new System.Drawing.Point(3, 16);
            this.grammarRichTextBox.Name = "grammarRichTextBox";
            this.grammarRichTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.grammarRichTextBox.Size = new System.Drawing.Size(184, 238);
            this.grammarRichTextBox.TabIndex = 0;
            this.grammarRichTextBox.Text = "";
            this.grammarRichTextBox.WordWrap = false;
            this.grammarRichTextBox.TextChanged += new System.EventHandler(this.grammarRichTextBox_TextChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.parseBtn, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.lexerGroupBox, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(205, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(196, 333);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // parseBtn
            // 
            this.parseBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parseBtn.Location = new System.Drawing.Point(3, 301);
            this.parseBtn.Name = "parseBtn";
            this.parseBtn.Size = new System.Drawing.Size(190, 29);
            this.parseBtn.TabIndex = 0;
            this.parseBtn.Text = "Compute Parse Table";
            this.parseBtn.UseVisualStyleBackColor = true;
            this.parseBtn.Click += new System.EventHandler(this.parseBtn_Click);
            // 
            // lexerGroupBox
            // 
            this.lexerGroupBox.Controls.Add(this.lexerRichTextBox);
            this.lexerGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lexerGroupBox.Location = new System.Drawing.Point(3, 3);
            this.lexerGroupBox.Name = "lexerGroupBox";
            this.lexerGroupBox.Size = new System.Drawing.Size(190, 292);
            this.lexerGroupBox.TabIndex = 1;
            this.lexerGroupBox.TabStop = false;
            this.lexerGroupBox.Text = "Lexical Analysis";
            // 
            // lexerRichTextBox
            // 
            this.lexerRichTextBox.DetectUrls = false;
            this.lexerRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lexerRichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lexerRichTextBox.Location = new System.Drawing.Point(3, 16);
            this.lexerRichTextBox.Name = "lexerRichTextBox";
            this.lexerRichTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.lexerRichTextBox.Size = new System.Drawing.Size(184, 273);
            this.lexerRichTextBox.TabIndex = 0;
            this.lexerRichTextBox.Text = "";
            this.lexerRichTextBox.WordWrap = false;
            this.lexerRichTextBox.Enter += new System.EventHandler(this.lexerRichTextBox_Enter);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.parsingGroupBox, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.translateBtn, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.parseTreeGroupBox, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(407, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(196, 333);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // parsingGroupBox
            // 
            this.parsingGroupBox.Controls.Add(this.parseTreeRichTextBox);
            this.parsingGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parsingGroupBox.Location = new System.Drawing.Point(3, 3);
            this.parsingGroupBox.Name = "parsingGroupBox";
            this.parsingGroupBox.Size = new System.Drawing.Size(190, 143);
            this.parsingGroupBox.TabIndex = 1;
            this.parsingGroupBox.TabStop = false;
            this.parsingGroupBox.Text = "Parsing Result";
            // 
            // parseTreeRichTextBox
            // 
            this.parseTreeRichTextBox.DetectUrls = false;
            this.parseTreeRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parseTreeRichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.parseTreeRichTextBox.Location = new System.Drawing.Point(3, 16);
            this.parseTreeRichTextBox.Name = "parseTreeRichTextBox";
            this.parseTreeRichTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.parseTreeRichTextBox.Size = new System.Drawing.Size(184, 124);
            this.parseTreeRichTextBox.TabIndex = 0;
            this.parseTreeRichTextBox.Text = "";
            this.parseTreeRichTextBox.WordWrap = false;
            this.parseTreeRichTextBox.Enter += new System.EventHandler(this.parseTreeRichTextBox_Enter);
            // 
            // translateBtn
            // 
            this.translateBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.translateBtn.Location = new System.Drawing.Point(3, 301);
            this.translateBtn.Name = "translateBtn";
            this.translateBtn.Size = new System.Drawing.Size(190, 29);
            this.translateBtn.TabIndex = 0;
            this.translateBtn.Text = "Translate To Intermediate Language";
            this.translateBtn.UseVisualStyleBackColor = true;
            this.translateBtn.Click += new System.EventHandler(this.translateBtn_Click);
            // 
            // parseTreeGroupBox
            // 
            this.parseTreeGroupBox.Controls.Add(this.parseTreeView);
            this.parseTreeGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parseTreeGroupBox.Location = new System.Drawing.Point(3, 152);
            this.parseTreeGroupBox.Name = "parseTreeGroupBox";
            this.parseTreeGroupBox.Size = new System.Drawing.Size(190, 143);
            this.parseTreeGroupBox.TabIndex = 2;
            this.parseTreeGroupBox.TabStop = false;
            this.parseTreeGroupBox.Text = "Parse Tree";
            // 
            // parseTreeView
            // 
            this.parseTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parseTreeView.Enabled = false;
            this.parseTreeView.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.parseTreeView.Location = new System.Drawing.Point(3, 16);
            this.parseTreeView.Name = "parseTreeView";
            this.parseTreeView.Size = new System.Drawing.Size(184, 124);
            this.parseTreeView.TabIndex = 0;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.saveBtn, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.cppGroupBox, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(609, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(197, 333);
            this.tableLayoutPanel5.TabIndex = 3;
            // 
            // saveBtn
            // 
            this.saveBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saveBtn.Location = new System.Drawing.Point(3, 301);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(191, 29);
            this.saveBtn.TabIndex = 0;
            this.saveBtn.Text = "Save Result";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // cppGroupBox
            // 
            this.cppGroupBox.Controls.Add(this.intermediateLanguageRichTextBox);
            this.cppGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cppGroupBox.Location = new System.Drawing.Point(3, 3);
            this.cppGroupBox.Name = "cppGroupBox";
            this.cppGroupBox.Size = new System.Drawing.Size(191, 292);
            this.cppGroupBox.TabIndex = 1;
            this.cppGroupBox.TabStop = false;
            this.cppGroupBox.Text = "Intermediate Language";
            // 
            // intermediateLanguageRichTextBox
            // 
            this.intermediateLanguageRichTextBox.DetectUrls = false;
            this.intermediateLanguageRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.intermediateLanguageRichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.intermediateLanguageRichTextBox.Location = new System.Drawing.Point(3, 16);
            this.intermediateLanguageRichTextBox.Name = "intermediateLanguageRichTextBox";
            this.intermediateLanguageRichTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.intermediateLanguageRichTextBox.Size = new System.Drawing.Size(185, 273);
            this.intermediateLanguageRichTextBox.TabIndex = 0;
            this.intermediateLanguageRichTextBox.Text = "";
            this.intermediateLanguageRichTextBox.WordWrap = false;
            this.intermediateLanguageRichTextBox.Enter += new System.EventHandler(this.intermediateLanguageRichTextBox_Enter);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "C++ Source Code|*.cpp";
            this.saveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog_FileOk);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Text Files|*.txt";
            this.openFileDialog.ReadOnlyChecked = true;
            this.openFileDialog.ShowReadOnly = true;
            this.openFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_FileOk);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 361);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TOPAT Final Project";
            this.Activated += new System.EventHandler(this.MainWindow_Activated);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.grammarGroupBox.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.lexerGroupBox.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.parsingGroupBox.ResumeLayout(false);
            this.parseTreeGroupBox.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.cppGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button doLexerBtn;
        private System.Windows.Forms.Button openFileBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button translateBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button parseBtn;
        private System.Windows.Forms.GroupBox lexerGroupBox;
        private System.Windows.Forms.RichTextBox lexerRichTextBox;
        private System.Windows.Forms.GroupBox parsingGroupBox;
        private System.Windows.Forms.RichTextBox parseTreeRichTextBox;
        private System.Windows.Forms.GroupBox cppGroupBox;
        private System.Windows.Forms.RichTextBox intermediateLanguageRichTextBox;
        private System.Windows.Forms.GroupBox grammarGroupBox;
        private System.Windows.Forms.RichTextBox grammarRichTextBox;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.GroupBox parseTreeGroupBox;
        private System.Windows.Forms.TreeView parseTreeView;
    }
}

