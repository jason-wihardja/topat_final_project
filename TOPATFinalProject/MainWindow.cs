﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace TOPATFinalProject {
    public partial class MainWindow : Form {
        private List<Token> tokens;
        private ParseTreeNode parseTree;

        public MainWindow() {
            InitializeComponent();

            initializeGUI();
            initializeVars();

            this.openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            this.saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        }

        public void initializeGUI() {
            this.lexerRichTextBox.Text = "";
            this.lexerRichTextBox.Enabled = false;
            this.parseTreeRichTextBox.Text = "";
            this.parseTreeRichTextBox.Enabled = false;
            this.intermediateLanguageRichTextBox.Text = "";
            this.intermediateLanguageRichTextBox.Enabled = false;
            this.parseTreeView.Nodes.Clear();
            this.parseTreeView.Enabled = false;

            this.parseBtn.Enabled = false;
            this.translateBtn.Enabled = false;
            this.saveBtn.Enabled = false;

            this.toolStripStatusLabel.Text = "";
        }

        public void initializeVars() {
            this.tokens = new List<Token>();
            this.parseTree = new ParseTreeNode("");
        }

        private void doLexerBtn_Click(object sender, EventArgs e) {
            if (this.grammarRichTextBox.Text.Trim().Equals("")) {
                this.grammarRichTextBox.Text = "";
                this.toolStripStatusLabel.Text = "No Input Detected!!";
                MessageBox.Show(this, "No Input Detected!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Boolean lanjut = true;

            // Do Lexical Analysis
            String input = Regex.Replace(this.grammarRichTextBox.Text, @"[\n\s]*", "").Trim();
            foreach (String token in new Regex(@"([\W])").Split(input)) {
                if (!token.Equals("")) {
                    if (new Regex(@"^\d+$").IsMatch(token)) {
                        this.tokens.Add(new Token("Digit", token));
                    } else if (new Regex(@"^\;$").IsMatch(token)) {
                        this.tokens.Add(new Token("Semicolon", token));
                    } else if (new Regex(@"^\{$").IsMatch(token)) {
                        this.tokens.Add(new Token("OpenBracket", token));
                    } else if (new Regex(@"^\}$").IsMatch(token)) {
                        this.tokens.Add(new Token("CloseBracket", token));
                    } else if (new Regex(@"^,$").IsMatch(token)) {
                        this.tokens.Add(new Token("Comma", token));
                    } else {
                        this.tokens.Add(new Token("Invalid", token));
                        lanjut = false;
                    }
                }
            }

            // Print Result
            foreach (Token t in tokens) {
                this.lexerRichTextBox.Text += t.category + ": \"" + t.value + "\"\n";
            }

            if (lanjut == true) {
                this.lexerRichTextBox.Enabled = true;
                this.parseBtn.Enabled = true;
                this.toolStripStatusLabel.Text = "Lexical Analysis: OK!";
            } else {
                this.lexerRichTextBox.Enabled = true;
                this.toolStripStatusLabel.Text = "Lexical Analysis: Error!!";
                MessageBox.Show(this, "Invalid Tokens Detected!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void parseBtn_Click(object sender, EventArgs e) {
            Boolean lanjut = true;

            // Do Parsing
            String input = Regex.Replace(this.grammarRichTextBox.Text, @"[\n\s]*", "").Trim() + "$";
            String stack = "S$";

            parseTree.name = "S";
            Stack<ParseTreeNode> currentNodes = new Stack<ParseTreeNode>();
            currentNodes.Push(parseTree);

            this.parseTreeRichTextBox.Text += String.Format("{0,-15}\t\t{1,0}\n", "Stack", "Input");
            for (Int32 i = 0; i < 80; i++) {
                this.parseTreeRichTextBox.Text += "-";
            }
            this.parseTreeRichTextBox.Text += "\n";
            this.parseTreeRichTextBox.Text += String.Format("{0,-15}\t\t{1,0}\n", stack, input);

            while (true) {
                Boolean isAnyRuleMatch = false;

                // Check with parse table
                switch (stack[0]) {
                    case 'S':
                        if (input[0] == '{') {
                            stack = stack.Replace("S", "{A}");
                            isAnyRuleMatch = true;

                            currentNodes.Peek().child.Add(new ParseTreeNode("{"));
                            currentNodes.Peek().child.Add(new ParseTreeNode("A"));
                            currentNodes.Peek().child.Add(new ParseTreeNode("}"));

                            ParseTreeNode temp = currentNodes.Pop();
                            currentNodes.Push(temp.child[1]);
                        }
                        break;

                    case 'A':
                        if (Char.IsDigit(input[0])) {
                            stack = stack.Replace("A", "BA");
                            isAnyRuleMatch = true;

                            currentNodes.Peek().child.Add(new ParseTreeNode("B"));
                            currentNodes.Peek().child.Add(new ParseTreeNode("A"));

                            ParseTreeNode temp = currentNodes.Pop();
                            currentNodes.Push(temp.child[1]);
                            currentNodes.Push(temp.child[0]);
                        } else if (input[0] == '}') {
                            stack = stack.Replace("A", "");
                            isAnyRuleMatch = true;

                            ParseTreeNode temp = currentNodes.Pop();
                            temp.child.Add(new ParseTreeNode("(empty)"));
                        } else if (input[0] == ',') {
                            stack = stack.Replace("A", "BA");
                            isAnyRuleMatch = true;

                            currentNodes.Peek().child.Add(new ParseTreeNode("B"));
                            currentNodes.Peek().child.Add(new ParseTreeNode("A"));

                            ParseTreeNode temp = currentNodes.Pop();
                            currentNodes.Push(temp.child[1]);
                            currentNodes.Push(temp.child[0]);
                        } else if (input[0] == ';') {
                            stack = stack.Replace("A", "BA");
                            isAnyRuleMatch = true;

                            currentNodes.Peek().child.Add(new ParseTreeNode("B"));
                            currentNodes.Peek().child.Add(new ParseTreeNode("A"));

                            ParseTreeNode temp = currentNodes.Pop();
                            currentNodes.Push(temp.child[1]);
                            currentNodes.Push(temp.child[0]);
                        }
                        break;

                    case 'B':
                        if (Char.IsDigit(input[0])) {
                            stack = stack.Replace("B", "CD;");
                            isAnyRuleMatch = true;

                            currentNodes.Peek().child.Add(new ParseTreeNode("C"));
                            currentNodes.Peek().child.Add(new ParseTreeNode("D"));
                            currentNodes.Peek().child.Add(new ParseTreeNode(";"));

                            ParseTreeNode temp = currentNodes.Pop();
                            currentNodes.Push(temp.child[1]);
                            currentNodes.Push(temp.child[0]);
                        } else if (input[0] == ',') {
                            stack = stack.Replace("B", "CD;");
                            isAnyRuleMatch = true;

                            currentNodes.Peek().child.Add(new ParseTreeNode("C"));
                            currentNodes.Peek().child.Add(new ParseTreeNode("D"));
                            currentNodes.Peek().child.Add(new ParseTreeNode(";"));

                            ParseTreeNode temp = currentNodes.Pop();
                            currentNodes.Push(temp.child[1]);
                            currentNodes.Push(temp.child[0]);
                        } else if (input[0] == ';') {
                            stack = stack.Replace("B", "CD;");
                            isAnyRuleMatch = true;

                            currentNodes.Peek().child.Add(new ParseTreeNode("C"));
                            currentNodes.Peek().child.Add(new ParseTreeNode("D"));
                            currentNodes.Peek().child.Add(new ParseTreeNode(";"));

                            ParseTreeNode temp = currentNodes.Pop();
                            currentNodes.Push(temp.child[1]);
                            currentNodes.Push(temp.child[0]);
                        }
                        break;

                    case 'C':
                        if (Char.IsDigit(input[0])) {
                            stack = stack.Replace("C", "N");
                            isAnyRuleMatch = true;

                            currentNodes.Peek().child.Add(new ParseTreeNode("N"));

                            ParseTreeNode temp = currentNodes.Pop();
                            currentNodes.Push(temp.child[0]);
                        } else if (input[0] == ',') {
                            stack = stack.Replace("C", "");
                            isAnyRuleMatch = true;

                            ParseTreeNode temp = currentNodes.Pop();
                            temp.child.Add(new ParseTreeNode("(empty)"));
                        } else if (input[0] == ';') {
                            stack = stack.Replace("C", "");
                            isAnyRuleMatch = true;

                            ParseTreeNode temp = currentNodes.Pop();
                            temp.child.Add(new ParseTreeNode("(empty)"));
                        }
                        break;

                    case 'D':
                        if (input[0] == ',') {
                            stack = stack.Replace("D", ",N");
                            isAnyRuleMatch = true;

                            currentNodes.Peek().child.Add(new ParseTreeNode(","));
                            currentNodes.Peek().child.Add(new ParseTreeNode("N"));

                            ParseTreeNode temp = currentNodes.Pop();
                            currentNodes.Push(temp.child[1]);
                        } else if (input[0] == ';') {
                            stack = stack.Replace("D", "");
                            isAnyRuleMatch = true;

                            ParseTreeNode temp = currentNodes.Pop();
                            temp.child.Add(new ParseTreeNode("(empty)"));
                        }
                        break;

                    case 'N':
                        if (Char.IsDigit(input[0])) {
                            stack = stack.Replace("N", "X");
                            isAnyRuleMatch = true;

                            currentNodes.Peek().child.Add(new ParseTreeNode("X"));

                            ParseTreeNode temp = currentNodes.Pop();
                            currentNodes.Push(temp.child[0]);
                        }
                        break;

                    case 'X':
                        if (Char.IsDigit(input[0])) {
                            stack = stack.Replace("X", input[0] + "X");
                            isAnyRuleMatch = true;

                            currentNodes.Peek().child.Add(new ParseTreeNode(input[0].ToString()));
                            currentNodes.Peek().child.Add(new ParseTreeNode("X"));

                            ParseTreeNode temp = currentNodes.Pop();
                            currentNodes.Push(temp.child[1]);
                        } else if (input[0] == ',') {
                            stack = stack.Replace("X", "");
                            isAnyRuleMatch = true;

                            ParseTreeNode temp = currentNodes.Pop();
                            temp.child.Add(new ParseTreeNode("(empty)"));
                        } else if (input[0] == ';') {
                            stack = stack.Replace("X", "");
                            isAnyRuleMatch = true;

                            ParseTreeNode temp = currentNodes.Pop();
                            temp.child.Add(new ParseTreeNode("(empty)"));
                        }
                        break;

                    case '{':
                    case '}':
                    case ',':
                    case ';':
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        if (stack[0] == input[0]) {
                            stack = stack.Substring(1);
                            input = input.Substring(1);
                            isAnyRuleMatch = true;
                        }
                        break;
                }

                if (isAnyRuleMatch) {
                    this.parseTreeRichTextBox.Text += String.Format("{0,-15}\t\t{1,0}\n", stack, input);
                }

                if (stack.Equals("$") && input.Equals("$")) {
                    this.parseTreeRichTextBox.Text += "\nResult: ACCEPTED";
                    lanjut = true;
                    break;
                }

                if (!isAnyRuleMatch) {
                    this.parseTreeRichTextBox.Text += "\nResult: NOT ACCEPTED";
                    lanjut = false;
                    break;
                }
            }

            if (lanjut == true) {
                this.parseTreeRichTextBox.Enabled = true;
                this.translateBtn.Enabled = true;
                this.toolStripStatusLabel.Text = "Generate Parsing Table: OK!";

                parseTreeView.Nodes.Add(parseTree.name);
                drawParseTree(parseTree, parseTreeView.Nodes[0].Nodes);
                this.parseTreeView.Enabled = true;
                this.parseTreeView.ExpandAll();
            } else {
                this.parseTreeRichTextBox.Enabled = true;
                this.toolStripStatusLabel.Text = "Generate Parsing Table: Error!!";
                MessageBox.Show(this, "The input is not accepted!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void drawParseTree(ParseTreeNode node, TreeNodeCollection tnc) {
            for (Int32 i = 0; i < node.child.Count; i++) {
                if (node.child[i].child.Count != 0) {
                    tnc.Add(node.child[i].name);
                    drawParseTree(node.child[i], tnc[i].Nodes);
                } else {
                    tnc.Add(node.child[i].name);
                }
            }
        }

        private void translateBtn_Click(object sender, EventArgs e) {
            // Do Translation
            Boolean isContinue = annotateParseTree(parseTree);

            if (isContinue == true) {
                this.intermediateLanguageRichTextBox.Enabled = true;
                this.saveBtn.Enabled = true;
                this.intermediateLanguageRichTextBox.Text = parseTree.val;
                this.toolStripStatusLabel.Text = "Translate to Intemediate Language: OK!";
            } else {
                this.intermediateLanguageRichTextBox.Enabled = true;
                this.toolStripStatusLabel.Text = "Translate to Intemediate Language: Error!!";
                MessageBox.Show(this, "Translate to Intemediate Language: Error!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Boolean annotateParseTree(ParseTreeNode node) {
            Boolean result = true;

            // Do Annotation
            switch (node.name) {
                case "S":
                    // S -> {A}
                    if (annotateParseTree(node.child[1])) {
                        node.val += "#include <iostream>\n";
                        node.val += "#include <cstdlib>\n";
                        node.val += "using namespace std;\n";
                        node.val += "\n";

                        node.val += "int main() {\n";
                        node.val += node.child[1].val;
                        node.val += "\tsystem(\"pause\");\n";
                        node.val += "\treturn 0;\n";
                        node.val += "}\n";
                    } else {
                        result = false;
                    }
                    break;

                case "A":
                    // A -> BA
                    if (node.child.Count == 2) {
                        if (annotateParseTree(node.child[0]) && annotateParseTree(node.child[1])) {
                            node.val = node.child[0].val + node.child[1].val;
                        } else {
                            result = false;
                        }
                    } else { // A -> (empty)
                        node.val = "";
                    }
                    break;

                case "B":
                    if (annotateParseTree(node.child[0]) && annotateParseTree(node.child[1])) {
                        if (node.child[0].val.Equals("") && !node.child[1].val.Equals("")) {
                            node.child[0].val = "Nol ";
                        } else if (!node.child[0].val.Equals("") && !node.child[1].val.Equals("")) {
                            if (!node.child[0].val[node.child[0].val.Length - 1].Equals(" ")) node.child[0].val += " ";
                        }

                        if (node.child[0].val.Contains(" ")) {
                            node.val = "\tcout << \"" + node.child[0].val.Remove(node.child[0].val.LastIndexOf(" ")) + node.child[1].val + "\" << endl;\n";
                        } else {
                            node.val = "\tcout << \"" + node.child[0].val + node.child[1].val + "\" << endl;\n";
                        }
                    } else {
                        result = false;
                    }
                    break;

                case "C":
                    // C -> N
                    if (node.child[0].name.Equals("N")) {
                        node.child[0].inh = "SebelumKoma";
                        if (annotateParseTree(node.child[0])) {
                            node.val = node.child[0].val;
                        } else {
                            result = false;
                        }
                    } else { // C -> (empty)
                        node.val = "";
                    }
                    break;

                case "D":
                    // D -> ,N
                    if (node.child.Count == 2) {
                        node.child[1].inh = "SetelahKoma";
                        if (annotateParseTree(node.child[1])) {
                            node.val = "Koma " + node.child[1].val;
                        } else {
                            result = false;
                        }
                    } else { // D -> (empty)
                        node.val = "";
                    }
                    break;

                case "N":
                    // N -> X
                    node.child[0].inh = node.inh;
                    if (annotateParseTree(node.child[0])) {
                        node.val = node.child[0].syn;
                    } else {
                        result = false;
                    }
                    break;

                case "X":
                    if (node.inh.Equals("SetelahKoma")) {
                        node.child[1].inh = "SetelahKoma";

                        switch (node.child[0].name) {
                            case "0":
                                node.val = "Nol";
                                break;

                            case "1":
                                node.val = "Satu";
                                break;

                            case "2":
                                node.val = "Dua";
                                break;

                            case "3":
                                node.val = "Tiga";
                                break;

                            case "4":
                                node.val = "Empat";
                                break;

                            case "5":
                                node.val = "Lima";
                                break;

                            case "6":
                                node.val = "Enam";
                                break;

                            case "7":
                                node.val = "Tujuh";
                                break;

                            case "8":
                                node.val = "Delapan";
                                break;

                            case "9":
                                node.val = "Sembilan";
                                break;
                        }

                        if (node.child[1].child.Count == 2) {
                            if (annotateParseTree(node.child[1])) {
                                node.val += " ";
                                node.val += node.child[1].val;
                            } else {
                                result = false;
                            }
                        }

                        node.syn = node.val;
                    } else {
                        if (node.child.Count == 2) {
                            if (annotateParseTree(node.child[1])) {
                                node.val += node.child[0].name;
                                node.val += node.child[1].val;
                            }
                            node.syn = terbilang1(UInt64.Parse(node.val));
                        }
                    }
                    break;
            }

            return result;
        }

        private String terbilang1(UInt64 p) {
            if (p < 12) return terbilang0(p);
            else if (p < 20) return terbilang1(p - 10) + "Belas ";
            else if (p < 100) return terbilang1(p / 10) + "Puluh " + terbilang1(p % 10);
            else if (p < 200) return "Seratus " + terbilang1(p - 100);
            else if (p < 1000) return terbilang1(p / 100) + "Ratus " + terbilang1(p % 100);
            else if (p < 2000) return "Seribu " + terbilang1(p - 1000);
            else if (p < 1000000) return terbilang1(p / 1000) + "Ribu " + terbilang1(p % 1000);
            else if (p < 1000000000) return terbilang1(p / 1000000) + "Juta " + terbilang1(p % 1000000);
            else if (p < 1000000000000) return terbilang1(p / 1000000000) + "Milyar " + terbilang1(p % 1000000000);
            else return terbilang1(p / 1000000000000) + "Triliun " + terbilang1(p % 1000000000000);
        }

        private String terbilang0(UInt64 p) {
            switch (p) {
                case 1: return "Satu ";
                case 2: return "Dua ";
                case 3: return "Tiga ";
                case 4: return "Empat ";
                case 5: return "Lima ";
                case 6: return "Enam ";
                case 7: return "Tujuh ";
                case 8: return "Delapan ";
                case 9: return "Sembilan ";
                case 10: return "Sepuluh ";
                case 11: return "Sebelas ";
                default: return "";
            }
        }

        private void openFileBtn_Click(object sender, EventArgs e) {
            this.openFileDialog.ShowDialog(this);
        }

        private void saveBtn_Click(object sender, EventArgs e) {
            this.saveFileDialog.ShowDialog(this);
        }

        private void openFileDialog_FileOk(object sender, CancelEventArgs e) {
            this.grammarRichTextBox.Text = File.ReadAllText(openFileDialog.FileName);
            this.toolStripStatusLabel.Text = "File Read: OK!";
        }

        private void saveFileDialog_FileOk(object sender, CancelEventArgs e) {
            File.WriteAllText(this.saveFileDialog.FileName, this.intermediateLanguageRichTextBox.Text, Encoding.ASCII);
            this.toolStripStatusLabel.Text = "File Save: OK!";
        }

        private void grammarRichTextBox_TextChanged(object sender, EventArgs e) {
            initializeGUI();
            initializeVars();
        }

        private void MainWindow_Activated(object sender, EventArgs e) {
            this.Width = Screen.PrimaryScreen.WorkingArea.Width - 100;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height - 100;
            this.CenterToScreen();
            this.grammarRichTextBox.Focus();
        }

        private void lexerRichTextBox_Enter(object sender, EventArgs e) {
            this.parseBtn.Focus();
        }

        private void parseTreeRichTextBox_Enter(object sender, EventArgs e) {
            this.translateBtn.Focus();
        }

        private void intermediateLanguageRichTextBox_Enter(object sender, EventArgs e) {
            this.saveBtn.Focus();
        }
    }
}
