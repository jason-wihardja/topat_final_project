﻿using System;
using System.Collections.Generic;

namespace TOPATFinalProject {
    class ParseTreeNode {
        public ParseTreeNode(String name) {
            this.name = name;
            this.inh = "";
            this.syn = "";
            this.val = "";
            this.child = new List<ParseTreeNode>();
        }

        public String name { get; set; }
        public String inh { get; set; }
        public String syn { get; set; }
        public String val { get; set; }
        public List<ParseTreeNode> child { get; set; }
    }
}
