﻿using System;

namespace TOPATFinalProject {
    class Token {
        public Token( String category, String value ) {
            this.category = category;
            this.value = value;
        }

        public String category { get; set; }
        public String value { get; set; }
    }
}
